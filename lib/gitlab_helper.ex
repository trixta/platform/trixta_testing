defmodule TrixtaTesting.GitlabHelper do
    require Logger
  
    @gitlab_timeout 120_000
    @gitlab_404_reminder_note "Note: If the file is not missing, this could mean the Private Token is empty or invalid"
    @last_remote_update_commit_id "last_remote_update_commit_id"
    @last_remote_update_timestamp "last_remote_update_timestamp"
    # Wait some time to space out requests to prevent triggering Gitlab's API call threshold
    @delay 100

    @tests_root_path "role_tests" # Path in a composition project where the role test definitions are located

    def fetch_tests_from_composition(repo_path, branch, gitlab_token) do
        repo_path |> URI.encode_www_form 
        |> file_list_url(branch)
        |> gitlab_get(gitlab_token)
        |> file_list_from_response()
        |> Enum.map(fn file_path -> file_contents_from_path(URI.encode_www_form(repo_path), file_path, branch, gitlab_token) end)
    end

    defp file_list_url(repo_path, branch) do
        "https://gitlab.com/api/v4/projects/#{repo_path}/repository/tree?path=#{URI.encode_www_form(@tests_root_path)}&ref=#{URI.encode_www_form(branch)}&recursive=true"
    end

    defp file_contents_url(repo_path, file_path, branch) do
        "https://gitlab.com/api/v4/projects/#{repo_path}/repository/files/#{URI.encode_www_form(file_path)}?ref=#{URI.encode_www_form(branch)}"
    end

    defp file_contents_from_path(repo_path, file_path, branch, gitlab_token) do
        file_contents_url(repo_path, file_path, branch)
        |> gitlab_get(gitlab_token)
        |> test_definition_from_response()
        # Each test file gets translated into a separate ExUnit module, namespaced by role and filename:
        |> Map.put("test_group_name", Path.basename(file_path) |> Path.rootname()) 
    end

    defp gitlab_get(url, gitlab_token) do
        HTTPotion.get(url,
            headers: [
            "Content-Type": "application/json",
            "Private-Token": gitlab_token
            ],
         timeout: @gitlab_timeout
        )
    end


    defp file_list_from_response(%HTTPotion.Response{status_code: 200, body: response_body}) when is_binary(response_body) do
        case Poison.decode(response_body) do
            {:ok, file_list} when is_list(file_list) ->
                file_list |> Enum.filter(fn item -> item["type"] === "blob" end) |> Enum.map(fn item -> item["path"] end)
            _ ->
                Logger.error("Could not decode gitlab response body into file list. Gitlab returned: #{inspect response_body}")
        end
    end

    defp file_list_from_response(resp) do
        Logger.error("Could not load list of composition test files from gitlab. Response: #{inspect resp}")
        []
    end

    defp test_definition_from_response(%HTTPotion.Response{status_code: 200, body: response_body}) when is_binary(response_body) do
        case Poison.decode(response_body) do
            {:ok, %{"content" => content}} -> test_definition_from_response(content)
            _ -> 
                Logger.error("Could not decode gitlab response body as json: #{inspect response_body}")
                :json_decode_error
        end
    end

    defp test_definition_from_response(encoded_content) when is_binary(encoded_content) do
        case Base.decode64(encoded_content) do
            {:ok, decoded_content} -> elixir_map_from_response(decoded_content)
            _ -> :base64_decoding_error
        end
    end

    defp test_definition_from_response(err) do
        Logger.error("Error while fetching test file from gitlab: #{inspect err}")
        :error_fetching_file
    end

    defp elixir_map_from_response(decoded_content) when is_binary(decoded_content) do
        # TODO: How safe is this? Alternatives?
        try do
            Code.eval_string(decoded_content) |> elem(0)
        rescue
            err ->
                Logger.error("Could not evaluate .ex test definition. Error: #{inspect err}")
                :elixir_eval_error
        end
    end
  
    @doc false
    defp content_from_response_body(response_body) do
      parsed_body = Poison.decode!(response_body)
      parsed_body["content"] |> Base.decode64!()
    end
  end
  