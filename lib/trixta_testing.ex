defmodule TrixtaTesting do
  @moduledoc """
  Documentation for TrixtaTesting.
  """

  @doc """
  Hello world.

  ## Examples

      iex> TrixtaTesting.hello()
      :world

  """
  def hello do
    :world
  end
end
