defmodule TrixtaTesting.TestMacros do
    @moduledoc """
    Macros to generate ExUnit tests from a test spec inside a composition project.
    """
    require Logger
    alias TrixtaUtils.Websockets.WebsocketClient
    alias TrixtaUtils.Serialisation.RoleMockSerialiser

    defmacro define_all_tests() do
        ensure_endpoint_valid()
        [_host, repo_path, _file, branch, gitlab_token] = split_trixta_seed()

        HTTPotion.start() # This is run at compile time, so the ibrowse supervision tree wouldn't have started yet. Start it manually.
        
        macro = TrixtaTesting.GitlabHelper.fetch_tests_from_composition(repo_path, branch, gitlab_token)
        |> build_all_tests()
        |> Macro.expand(__CALLER__)

        macro |> Macro.to_string |> IO.puts

        macro
    end

    defp split_trixta_seed() do
        System.get_env("TRIXTA_SEED") |> String.split(";")
    end

    defp ensure_endpoint_valid() do
        parsed_uri = (System.get_env("TEST_SPACE_ENDPOINT") || "") |> URI.parse()
        case :inet.gethostbyname(to_char_list parsed_uri.host) do
            {:ok, _} -> :ok
            _ -> raise ArgumentError, "Invalid test space endpoint or host cannot be reached. Please check your TEST_SPACE_ENDPOINT environment variable."
        end
    end

    defp build_all_tests(test_definitions) when is_list(test_definitions) do
        quote do
            unquote_splicing(
                test_definitions |> Enum.map(fn def -> 
                    build_role_test(def) 
                end)
            )
        end
    end

    defp build_all_tests(test_definitions) do
        Logger.error("Could not compile tests. Invalid definitions: #{inspect test_definitions}")
        nil
    end

    defp build_role_test(%{"role" => test_role, "test_cases" => cases} = test) when is_list(cases) do
        # An ExUnit module is defined for every test file in the composition project.
        # The modules are namespaced by role name and file name.
        module_name = Module.concat([TrixtaTesting.Tests, Macro.camelize(test_role), Macro.camelize(test["test_group_name"] || "test")])
        quote do
            defmodule unquote(module_name) do
                use ExUnit.Case, async: false

                unquote_splicing(
                    cases |> Enum.map(fn test_case -> 
                       build_test_case(test_case, test)
                    end)
                )
            end
        end
    end

    defp build_role_test(test) do
        Logger.error("Cannot compile role test from definition: #{inspect test}")
        nil
    end

    defp build_test_case(%{"description" => desc} = test_case, entire_test) do
        role = test_case["role"] || entire_test["role"]
        agent_id = test_case["agent_id"] || entire_test["agent_id"]
        agent_password = test_case["agent_password"] || entire_test["agent_password"]

        message = "Sending test action #{test_case["action"]} on role #{role} . Using agent id: #{agent_id}"
        quote do
            test unquote(desc) do
                IO.puts(unquote(message))
                %{:result => result} = WebsocketClient.send_action_to_space(
                    System.get_env("TEST_SPACE_ENDPOINT"),
                    unquote("space:#{role}"),
                    unquote(test_case["action"]),
                    unquote(test_case["payload"] |> RoleMockSerialiser.make_serialisable() |> Macro.escape),
                    %{
                    "identity" => unquote(agent_id),
                    "password" => unquote(agent_password)
                    }
                )
                
                assert unquote(test_case["expected_reply"]) = result  # We have to escape so that AST-like structures (maps, etc) don't get confused
            end
        end
    end

    defp build_test_case(test_case, _) do
        Logger.error("Could not build test case. Invalid test case definition: #{inspect test_case}")
        nil
    end
end