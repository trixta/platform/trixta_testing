# TrixtaTesting

> **This library reads flow integration test definitions from gitlab, then generates and runs ExUnit test cases for them.**

[![pipeline status](https://gitlab.com/trixta/platform/trixta_testing/badges/master/pipeline.svg)](https://gitlab.com/trixta/platform/trixta_testing/commits/master)
[![coverage report](https://gitlab.com/trixta/platform/trixta_testing/badges/master/coverage.svg)](https://gitlab.com/trixta/platform/trixta_testing/commits/master)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](code-of-conduct.md)
[![Maintenance](https://img.shields.io/maintenance/yes/2019.svg)]()
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `trixta_testing` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:trixta_testing, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/trixta_testing](https://hexdocs.pm/trixta_testing).

## Usage

### Running tests

1. Set the TEST_SPACE_ENDPOINT environment variable to the space's websocket endpoint against which you want to run tests, e.g.
`TEST_SPACE_ENDPOINT=wss://localhost:4000/socket/websocket`

2. Set the TRIXTA_SEED environment to the gitlab composition repo and branch where your tests are defined, e.g.
`TRIXTA_SEED=gitlab.com;trixta/composition/tracker;seed;feature/flowIntegrationTests;gitlab_token_here`
This works the same as in trixta_space, except that the host is hard-coded to gitlab.com at the moment instead of using the one from the variable. Also, the seed file doesn't really matter when running tests.

3. Run `mix test`
If everything is set up correctly, the harness will fetch all files in the `role_tests` directory of your composition project, and then generate and run a test for each of them.

## Contributing

MRs accepted. See [CONTRIBUTING](./CONTRIBUTING.md).

## License

© Trixta Inc. See [LICENSE](./LICENSE).
