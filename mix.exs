defmodule TrixtaTesting.MixProject do
  use Mix.Project

  def project do
    [
      app: :trixta_testing,
      version: "0.1.1",
      elixir: "~> 1.9",
      elixirc_paths: ["lib", "test"],
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      aliases: aliases(),
      name: "TrixtaTesting",
      docs: [main: "TrixtaTesting", extras: ["README.md"]]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:credo, "~> 1.5.6", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21", only: [:dev, :test], runtime: false},
      {:trixta_utils, path: "../trixta_utils"},
      {:httpotion, "~> 3.1.3"},
      {:poison, "~> 4.0"}
    ]
  end

  def aliases do
    [
      review: ["hex.audit", "hex.outdated", "credo --strict"]
    ]
  end

  defp description() do
    "The Trixta Testing System."
  end

  defp package() do
    [
      licenses: ["AGPL-3.0-or-later"],
      links: %{"GitLab" => "https://gitlab.com/trixta/platform/trixta_testing"}
    ]
  end
end
